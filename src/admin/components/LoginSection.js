export default{
    template:`
    <div class="login_section section" v-show="visible">
        <div class="login_section__form">
            <p v-if="willCreateAdmin">There is no admin in the system, let's create one.</p>
            <label>Username</label>
            <input autocomplete="0" :disabled="willCreateAdmin" v-model="form.username"/>
            <label>Password</label>
            <input autocomplete="0" type="password" v-model="form.password"/>
            <div class="login_section__form__remember">
                <input type="checkbox" v-model="rememberCredentials" />
                <label>Remember credentials?</label>
            </div>
            <div>
                <button v-if="!willCreateAdmin" class="login" @click="login">Login</button>
                <button v-if="willCreateAdmin" class="create" @click="create">Create</button>
            </div>
        </div>
    </div>
    `,
    data(){
        return {
            rememberCredentials:false,
            willCreateAdmin:false,
            form:{
                username:'',
                password:''
            }
        }
    },
    async created(){
        this.willCreateAdmin = !(await this.$fql('hasAdmin',{},{
            namespace:"diarios.auth"
        }))
        if(this.willCreateAdmin){
            this.form.username='admin'
        }
        let settings = (await window.localforage.getItem('diarios.login'))||{}
        this.rememberCredentials= settings.rememberCredentials||false
        if(this.rememberCredentials){
            let auth = (settings.auth&&atob(settings.auth)||"").split('###')
            this.form.username = auth[0]
            this.form.password = auth.length>1&&auth[1]
        }
    },
    computed:{
        visible(){
            return this.$root.section ==='login'
        }
    },
    methods:{
        async login(){
            let session = await this.$fql('login',{...this.form},{
                namespace:'diarios.auth'
            })
            window.localforage.setItem('diarios.session', session)
            window.localforage.setItem('diarios.login',{
                rememberCredentials: this.rememberCredentials,
                auth: this.rememberCredentials ? btoa(this.form.username+'###'+this.form.password) : ""
            })
            this.$root.onLoginSuccess()
        },
        async create(){
            await this.$fql('createAdmin',{...this.form},{
                namespace:'diarios.auth'
            })
            this.willCreateAdmin=false
        }
    }
}