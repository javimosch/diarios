export default {
    props:['value'],
    template: `
    <div>
        <input :disabled="true" :value="address.formatted||''"
        :style="{marginBottom:'15px'}"
        />
        <p v-show="!initialized">
        Loading...
        </p>
        <div class="leaflet_address">
            <div class="map" :id="id"></div>
        </div>
    </div>
    `
    ,
    data() {
        return {
            id: `map_` + Date.now(),
            initTries: 0,
            address: this.value || {},
            initialized:false,
        }
    },
    watch:{
        value(){
            if(typeof this.value === 'object' && !!this.value.formatted){
                this.address = {
                    ...this.value
                }
            }
        }
    },
    mounted() {
        this.lazyInit()
    },
    methods: {
        async lazyInit() {
            let alreadyLoaded = await this.loadDepsOnce()
            if (alreadyLoaded) {
                this.initialize()
            } else {
                setTimeout(() => {
                    try {
                        this.initialize()
                    } catch (err) {
                        console.warn(err)
                        this.initTries++
                        if (this.initTries < 10) {
                            this.lazyInit()
                        }
                    }
                }, 1000)
            }
        },
        initialize() {
            let self = this
            var map = L.map(this.id).setView([40.91, -96.63], 4);
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="https://osm.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);
            var searchControl = L.esri.Geocoding.geosearch().addTo(map);
            var results = L.layerGroup().addTo(map);
            this.initialized=true
            searchControl.on('results', function (data) {
                console.info(data)
                let address = self.address = {
                    streetNumber: data.results[0].properties.AddNum || "",
                    street: data.results[0].properties.StAddr || "",
                    city: data.results[0].properties.City || "",
                    department: data.results[0].properties.Subregion || "",
                    Region: data.results[0].properties.Region || "",
                    city: data.results[0].properties.City || "",
                    country: data.results[0].properties.Country || "",
                    zipcode: data.results[0].properties.Postal || "",
                    formatted: data.results[0].properties.Place_addr || "",
                    meta:{
                        bounds: data.results[0].bounds,
                        latlng: data.results[0].latlng,
                        properties: {
                            ...data.results[0].properties
                        }
                    }
                }
                self.$emit('input',{
                    ...address
                })
                results.clearLayers();
                for (var i = data.results.length - 1; i >= 0; i--) {
                    results.addLayer(L.marker(data.results[i].latlng));
                }
            });
        },
        async loadDepsOnce() {
            if (window._geo_initialized) {
                return true
            } else {
                window._geo_initialized = true
            }
            console.log('Loading geo deps')
            document.head.innerHTML += `
        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin=""/>
        <link rel="stylesheet" href="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.css"
        integrity="sha512-IM3Hs+feyi40yZhDH6kV8vQMg4Fh20s9OzInIIAc4nx7aMYMfo+IenRUekoYsHZqGkREUgx0VvlEsgm7nCDW9g=="
        crossorigin="">
        <style>
        .leaflet_address { margin:0; padding:0; 
        width: 100%;
        min-height:200px;
        position:relative;
        }
        .leaflet_address .map { position: absolute; top:0; bottom:0; right:0; left:0; }
    </style>
        `
            async function addScript(filename,waitForKey) {
                console.log('addScript...')
                var head = document.getElementsByTagName('head')[0];
                var script = document.createElement('script');
                script.src = filename;
                script.type = 'text/javascript';
                head.append(script);
                let start = Date.now()
                return new Promise((resolve,reject)=>{
                    function check(){
                        if(window[waitForKey]){
                            resolve()
                        }else{
                            if(Date.now()-start>5000){
                                console.warn('addScript timeout: wait for key', waitForKey)
                                resolve()
                                return
                            }
                            setTimeout(()=>check(),20)
                        }
                    }
                    check()
                })
            }
            await addScript('https://unpkg.com/leaflet@1.7.1/dist/leaflet.js',"L")
            await addScript('https://unpkg.com/esri-leaflet@2.5.0/dist/esri-leaflet.js',"Task")
            await addScript('https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.js')
            /*   document.head.innerHTML += `
           <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
           integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
           crossorigin=""></script>
           <!-- Load Esri Leaflet from CDN -->
           <script src="https://unpkg.com/esri-leaflet@2.5.0/dist/esri-leaflet.js"
           integrity="sha512-ucw7Grpc+iEQZa711gcjgMBnmd9qju1CICsRaryvX7HJklK0pGl/prxKvtHwpgm5ZHdvAil7YPxI1oWPOWK3UQ=="
           crossorigin=""></script>
           <!-- Load Esri Leaflet Geocoder from CDN -->
           <script src="https://unpkg.com/esri-leaflet-geocoder@2.3.3/dist/esri-leaflet-geocoder.js"
           integrity="sha512-HrFUyCEtIpxZloTgEKKMq4RFYhxjJkCiF5sDxuAokklOeZ68U2NPfh4MFtyIVWlsKtVbK5GD2/JzFyAfvT5ejA=="
           crossorigin=""></script>
               `*/
            return false
        }
    }
}