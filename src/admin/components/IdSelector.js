export default{
    props:{
        value:String,
        fqlRoute:String
    },
    template:`<div>
        <input :disabled="true" @click="openTable" :value="text" />
        <button @click="openTable">Set</button>
        <div v-show="showTable" class="pick_up_popup" @click="closeTable">
            <div v-for="item in items" :key="item._id" @click="select($event,item)" class="pick_up_popup__item">
                {{item.text}}
            </div>
        </div>
    </div>`,
    data(){
        return{
            text: this.value || "",
            showTable:false,
            items:[]
        }
    },
    watch:{
        value(){
            this.text=this.value
        }
    },
    methods:{
        async openTable(){
            this.showTable=true
            this.items = (await this.$fql(this.fqlRoute)||[])
        },
        closeTable(){
            this.showTable=false
        },
        select(e,item){
            e.stopPropagation()
            e.preventDefault()
            this.text = item._id
            this.$emit('input',item._id)
            this.showTable=false
        }
    }
}