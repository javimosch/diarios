import ThemeEditorMixin from '/admin/mixins/theme-editor.js'

export default {
    mixins: [ThemeEditorMixin],
    props: ['parent', 'rightPopupStyle'],
    template: `
    <div class="themes_view" ref="root">
        <div class="section" v-show="parent.section=='themes'">
            <div class="themes_item" v-for="(item,index) in themes" :key="index"
            @click="editTheme(item)"
            >
                {{item}}
            </div>
        </div>

        <div class="rightSidebar full theme_details" v-show="parent.rightSidebar==='editTheme'">
            <div class="theme_files">
                <button :disabled="!file.dirty" @click="save">Save</button>
                <button @click="back">Back</button>
                <ul>
                    <li
                    v-for="(item,index) in pugFiles" :key="index"
                    class="theme_file"
                    @click="editFile(item)"
                    >
                        {{item}}
                    </li>
                </ul>
            </div>
            <div ref="aceEditor" class="ace_editor"></div>
        </div>
        <button class="themes_view__preview"
        v-show="selectedTheme.name"
        @click="preview">Preview</button>
        <div class="rightPopup"  :style="rightPopupStyle"  v-show="parent.rightPopup==='popupPreview'">
            <div class="rightPopup__content">
                <input type="range" value="100" min="0" step=5 max="80" v-model="parent.innerRightPopupStyle.width" style="width:100%">
                <p v-show="pages.length===0 && file.path===''">(Edit a file first)</p>
                
                <div v-show="pages.length===0 && file.path!==''">
                <label>Page</label>

                <id-selector
                ref="idSelector"
                v-model="customPagePath"
                fqlRoute="getPagePathsForSelector"
                >
                </id-selector>
                </div>

                <select v-model="page" v-show="pages.length>0">
                    <option v-for="item in pages" :key="item._id" :value="item" v-text="item.name">
                    </option>
                </select>
                <iframe v-show="page._id || customPagePath":src="getPreviewPath">
                </iframe>
            </div>
        </div>
    </div>
    `,
    data() {
        return {
            themes: [],
            page: {
                path: ""
            },
            customPagePath:""
        }
    },
    async created() {
        this.themes = await this.$fql('getThemeFolders')
    },
    computed:{
        getPreviewPath(){
            return (this.page.path||this.customPagePath||'')+'/?iframe=1&pugErrors=1'
        }
    },
    methods: {
        refreshPreview(timeout) {
            if(timeout){
                if(this.previewTimeout){
                    window.clearTimeout(this.previewTimeout)
                }
                this.previewTimeout = setTimeout(()=>this.refreshPreview(),timeout)
                return
            }
            this.$refs.root.querySelector('iframe').src = this.getPreviewPath
        },
        preview() {
            this.$emit('preview', 'popupPreview')
        }
    },
    watch:{
        page(){
            this.refreshPreview(1000)
        }
    }
}