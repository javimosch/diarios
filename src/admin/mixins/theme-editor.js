export default {
    data() {
        return {
            pages:[],
            selectedTheme: {
                name: '',
                files: []
            },
            file: {
                dirty:false,
                path: '',
                contents: ''
            }
        }
    },
    watch:{
        file:{
            async handler(){
                if(this.file && this.file.path){
                    this.pages = await this.$fql('getPagesMatchingTemplate',[
                        this.file.path.substring(this.file.path.lastIndexOf('/')).split('.')[0]
                    ])
                    if(this.pages.length>0){
                        if(window.innerWidth>1024){
                            this.preview()
                        }
                        this.page = this.pages[0]
                    }else{
                        this.page = {
                            path:''
                        }
                    }
                }
            },
            deep:true
        }
    },
    methods: {
        async editTheme(folderName) {
            console.log("editTheme")
            this.selectedTheme = {
                name: folderName,
                files: await this.$fql('getThemeFiles', [folderName])
            }
            this.parent.rightSidebar = "editTheme"
        },
        async editFile(filePath) {
            this.file = {
                ...this.file,
                path: filePath,
                contents: await this.$fql('getFileContents', [this.selectedTheme.name, filePath])
            }
            this.aceEditor.session.setValue(this.file.contents, -1)
        },
        async save() {
            await this.$fql('saveFile', [this.selectedTheme.name, {
                ...this.file,
                contents: this.aceEditor.session.getValue("")
            }])
            this.file.dirty=false
            this.refreshPreview(1000)
        },
        back() {
            this.aceEditor.session.setValue("")
            this.file = {
                dirty:false,
                path: '',
                contents: ''
            }
            this.selectedTheme = {
                name: '',
                files: []
            }
            setTimeout(() => {
                this.parent.rightSidebar = ""
                this.parent.rightPopup= ''
            }, 100)
        },
        saveDelay(timeout = 0){
            if(timeout){
                if(this.saveTimeout){
                    window.clearTimeout(this.saveTimeout)
                }
                this.saveTimeout = setTimeout(()=>this.saveDelay(),timeout)
                return
            }
            this.save()
        }
    },
    computed: {
        pugFiles() {
            return this.selectedTheme.files.filter(f => f.indexOf(".pug") !== -1)
        }
    },
    mounted() {
        this.aceEditor = ace.edit(this.$refs.aceEditor);
        this.aceEditor.setTheme("ace/theme/monokai");
        this.aceEditor.session.setMode("ace/mode/jade");
        this.aceEditor.session.setTabSize(2);
        this.aceEditor.session.setUseWrapMode(true);
        this.aceEditor.on("input", ()=> {
            this.file.dirty = true
            this.saveDelay(1000)
        });
    }
}