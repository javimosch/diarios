const qsParams = new URLSearchParams(window.location.search);
qsParams.updateUrl = () => {
    if (history.pushState) {
        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname.split('?')[0] + "?" + qsParams.toString();
        window.history.pushState({ path: newurl }, '', newurl);
    }
}
qsParams.setP = qsParams.set
qsParams.set = (n, v) => {
    qsParams.setP(n, v)
    qsParams.updateUrl()
}
qsParams.deleteP = qsParams.delete
qsParams.delete = n => {
    qsParams.deleteP(n)
    qsParams.updateUrl()
}

export default qsParams