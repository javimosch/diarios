import { fql } from '/admin/common/fql.js'
import { Vue } from '/admin/common/vue.js'


Vue.use({
    install() {
        let firstTimeSync = false
        let page = {}
        Vue.prototype.$settings = {
            async getParam(name) {
                if(!firstTimeSync){
                    await syncParams()
                    firstTimeSync=true
                }
                return (page.sections.find(s => s.name == name) || {}).value || ""
            },
            async saveParam(payload = {}) {
                await fql('saveParam', [payload])
                await syncParams()
            },
            syncParams
        };

        async function syncParams() {
            page = (await fql('getPageByName', '_settings')) || {}
            console.log('syncParams',page)
        }
        Vue.prototype.$root = {}
    }
})
