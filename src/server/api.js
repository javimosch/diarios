const { text } = require('express')
let mongoose = require('mongoose')
var m = n => mongoose.model(n)
module.exports = async app => {
    const funql = require('funql-api')
    funql.middleware(app, {
        /*defaults*/
        getMiddlewares: [],
        postMiddlewares: [],
        allowGet: true,
        allowOverwrite: false,
        attachToExpress: true,
        allowCORS: true,
        api: {
            diarios:{
                user:{
                    countUsers(){
                        return m('User').count({})
                    },
                },
                auth:{
                    async createAdmin(form){
                        if(form.password&&!(await app.api.diarios.auth.hasAdmin())){
                            await m('User').create({
                                username:"admin",
                                password: require('md5')(form.password)
                            })
                        }
                        return true
                    },
                    async hasAdmin(){
                        return (await m('User').count({
                            username:'admin'
                        })) >= 1
                    },
                    async login(form){
                        if(form.username && form.password && await m('User').findOne({
                            username: form.username,
                            password: require('md5')(form.password)
                        })){
                            let token = await app.api.diarios.auth.signToken({
                                username: form.username
                            })
                            return {
                                token,
                                username: form.username
                            }
                        }
                        return {
                            err:"INVALID_CREDENTIALS"
                        }
                    },
                    signToken(data = {}){
                        if(this.req){
                            return false
                        }
                        return new Promise((resolve,reject)=>{
                            require('jsonwebtoken').sign({
                                exp: Math.floor(Date.now() / 1000) + (60 * 60),
                                data,
                            }, process.env.SESSION_SECRET||'secret',(err, token)=>{
                                if(err){
                                    reject(err)
                                }else{
                                    resolve(token)
                                }
                            });
                        })
                    }
                }
            },
            async saveFile(themeName, file) {
                if(!themeName){
                    return{
                        err:"THEME_REQUIRED"
                    }
                }
                if(!file||!file.path){
                    return {
                        err:"FILE_REQUIRED"
                    }
                }
                console.log('saveFile',require('path').join(process.cwd(), 'templates', themeName, file.path), file.contents)
                await require('sander').writeFile(require('path').join(process.cwd(), 'templates', themeName, file.path), file.contents)
                return true
            },
            async getFileContents(themeName, filePath) {
                return (await require('sander').readFile(require('path').join(process.cwd(), 'templates', themeName, filePath))).toString('utf-8')
            },
            async getThemeFiles(name) {
                const getFiles = require('simply-get-files');
                return await getFiles(require('path').join(process.cwd(), 'templates', name));
            },
            async getThemeFolders() {
                let path = require('path')
                let dirs = await require('sander').readdir(path.join(process.cwd(), 'templates'))
                return dirs.filter(d => d.indexOf('.') == -1)
            },
            async uploadSingleImage(fields, files) {
                console.log('uploadSingleImage', {
                    fields, files
                })
                let today = require('moment')().format('DD-MM-YYY')
                let name = `image_${Date.now()}_${fields.filename}`

                let publicPath = require('path').join('upload', today, name)
                let writePath = require('path').join(process.cwd(), 'public/upload', today, name)
                console.log('Will write asset at', {
                    path: writePath
                })
                const sander = require('sander')
                await sander.copyFile(files.file.path).to(writePath, {
                    //mode: 0o666//require('fs').constants.COPYFILE_FICLONE
                })
                let payload = {
                    name: fields.filename,
                    type: fields.type || 'unknown',
                    path: writePath,
                    publicPath
                }
                let page = await m('Page').findById(fields.pageId)
                let index = page.assets.findIndex(a => a.name == name)
                if (index !== -1) {
                    console.warn('Removing existing asset at ', {
                        path: page.assets[index].path
                    })
                    try {
                        await sander.rimraf(page.assets[index].path)
                    } catch (err) {
                        console.log(err.stack)
                    }
                    page.assets[index] = {
                        path: writePath
                    }
                } else {
                    page.assets.push(payload)
                }
                await page.save()
                return payload
            },
            async removePage(page) {
                await m('Page').findByIdAndRemove(page._id)
                return true
            },
            async removePageSection(pageId, sectionId) {
                let p = await m('Page').findById(pageId)
                p.sections.splice(p.sections.findIndex(s => s._id == sectionId), 1)
                await p.save()
                return true
            },
            async getPages() {
                return await m('Page').find({});
            },
            async getPage(_id) {
                return await m('Page').findById(_id);
            },
            async getPageByName(name){
                return await m('Page').findOne({
                    name
                });
            },
            async getPagesMatchingTemplate(template){
                return await m('Page').find({
                    template
                }).select('_id name path')
            },
            async saveParam(payload = {}){
                let settings = await m('Page').findOne({
                    name: '_settings'
                })
                let match = settings.sections.findIndex(s=>s.name==payload.name)
                if(match!==-1){
                    settings.sections[match] = {
                        ...settings.sections[match],
                        ...payload
                    }
                }else{
                    settings.sections.push(payload)
                }
                await settings.save()
                return true
            },
            async getPageTemplatesForSelector(){
                return (await m('Page').find({
                    isCore:false,
                    isTemplate:true
                })).map(p=>({
                    text: p.name,
                    _id: p._id
                }))
            },
            async getPagePathsForSelector(){
                return (await m('Page').find({
                    isCore:false,
                    isTemplate:false
                })).map(p=>({
                    text: p.name,
                    _id: p.path
                }))
            },
            async savePage(page) {

                if(page.isTemplate){
                    page.path = require('uniqid')(`/template-`);
                }else{
                    page.isTemplate = false
                }
                if (!page.path || !page.name) {
                    return {
                        err: "BAD_REQUEST"
                    }
                }

                if (await m('Page').findOne({
                    path: page.path,
                    name: {
                        $ne: page.name
                    }
                })) {
                    return {
                        err: "PATH_MISTMACH"
                    }
                }

                if (typeof page.tags === 'string') {
                    page.tags = page.tags.trim().split(',').map(t => t.trim())
                }

                if (! await m('Page').findOne({
                    name: page.name
                })) {
                    await m('Page').create({
                        ...page
                    })
                    return true
                }

                await m('Page').updateOne({
                    name: page.name
                }, {
                    $set: {
                        ...page 
                    }
                })
                return true
            },
            async savePages(pages) {
                console.log('savePages', {
                    pages
                })
                await Promise.all(pages.map(p => {
                    return app.api.savePage(p)
                }))
                return true
            },
            async loadFixtures() {
                console.log('loadFixtures')

                var ensurePage = async (name, sections, options = {}) => {
                    await m('Page').findOneAndUpdate({
                        name
                    }, {
                        name,
                        isCore: options.isCore !== undefined ? options.isCore : false
                    }, {
                        upsert: true
                    })
                    let page = await m('Page').findOne({ name })
                    for (var x = 0; x < sections.length; x++) {
                        if (!page.sections.find(s => s.name == sections[x].name)) {
                            page.sections.push({
                                ...sections[x]
                            })
                            await page.save()
                        }
                    }
                    return page
                }

                /*
                let navbar = await ensurePage('navbar', [{
                    name: 'navbar_title',
                    type: "text",
                    value: `Bon Beau en Bauges`
                }])

                let home = await ensurePage('home', [{
                    name: 'header_message',
                    type: "text",
                    value: `Tout en bauges<br>	Bon, Beau, Local !`
                },{
                    name:"presentation_message",
                    type:'text',
                    value:`La vallee des Bauges fourmille de richesses ! Des producteurs soucieux de preserver le gout des bonnes choses tout autant que les sols et les paysages, des artisants qui perpetuent et revisitent les savoir faire traditionnels, des points de rencontre pour apprendre et partager. Nous avons hate de vous faire decouvrir tout ca au travers de ces articles. Suivez le guide !`
                }])

                let about = await ensurePage('about',[{
                    name:'about_message',
                    type:'text',
                    value:`Habitant les bauges depuis l’enfance, je suis étonnée et heureuse du dynamisme de la région depuis quelques années. Associations, artisans, producteurs respectueux de l’environnement, passionnés de nature, les bauges fourmillent de nouvelles initiatives qui font le bonheur de ses habitants comme des touristes.

                    Le bouche à bouche étant leur principale source de communication, certaines adresses ne sont pourtant pas connues des personnes des passages, des nouveaux venus ou même parfois des habitants de toujours qui n’ont pas eu vent des derniers bons plans des bauges !
                    
                    J’ai donc souhaité par ce blog apporter ma contribution au développement de cette vallée que j’affectionne tant en mettant en valeur ceux qui y vivent et produisent de façon responsable pour notre bonheur.`
                }])
                */

                let _settings = await ensurePage('_settings', [{
                    name: "templateEngine",
                    type: 'text',
                    value: 'pug'
                }, {
                    name: "default_page_template",
                    type: "text",
                    value: "page"
                }, {
                    name: "theme",
                    type: "text",
                    value: "basic"
                }], {
                    path: '/settings',
                    isCore: true
                })


                return await m('Page').find({});
            },
            backoffice: {
                getUsers() {
                    return ['Juan', 'Paco']
                }
            }
        }
    })
}