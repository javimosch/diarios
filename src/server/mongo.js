const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({ 
    username: {
        type:String,
        unique:true
    }, 
    password: String,
    email: String,
    phone: String
});
const User = mongoose.model('User', UserSchema);

const PageAssetSchema = new mongoose.Schema({
    name: {
        type:String
    }, 
    type:String,
    path: String,
    publicPath: String
});

const PageSectionSchema = new mongoose.Schema({
    name: {
        type:String
    }, 
    type: {
        type:String,
        required: true,
        enum: ['text','html','address', 'single_image','object']
    },
    value:Object
});
const PageSection = mongoose.model('PageSection', PageSectionSchema);

const PageSchema = new mongoose.Schema({ 
    name: 'string', 
    path: {
        type:String,
        unique:true
    },
    tags:{
        type: [String],
        default:[]
    },
    isCore: {
        type: Boolean,
        default:false
    },
    template: 'string', //Theme template
    isTemplate: Boolean,
    assets: [PageAssetSchema],
    sections: {
        type: [PageSectionSchema],
        default: []
    } });
const Page = mongoose.model('Page', PageSchema);

module.exports = async app => {
    return new Promise(async (resolve, reject) => {

        mongoose.connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true });
        console.log('mongo', process.env.MONGO_URI)

        mongoose.connection.once('open', function () {
            // we're connected!
            console.log('mongo ok')

            const session = require('express-session');
            const MongoStore = require('connect-mongo')(session);

            app.use(session({
                secret: process.env.SESSION_SECRET,
                saveUninitialized: true,
                resave: true,
                store: new MongoStore({ mongooseConnection: mongoose.connection })
            }));
            resolve()
        });
    })

}