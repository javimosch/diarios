require('dotenv').config({
    silent: true
})
const express = require('express')
const app = express()
const PORT = process.env.PORT || 3000
const path = require("path")
const api = require('./src/server/api');

(async () => {

    app.use((req, res, next) => {
        if (req.url.indexOf('.pug') !== -1) {
            res.status(403).send()
        } else {
            next()
        }
    })

    await require('./src/server/mongo')(app)

    app.compile = require('./src/server/compile')
    app.use('/', express.static(path.join(process.cwd(), 'public')))
    app.use('/', express.static(path.join(process.cwd(), 'templates')))
    app.use('/admin/public', express.static(path.join(process.cwd(), 'src', 'admin', 'public')))
    app.use('/admin/components', express.static(path.join(process.cwd(), 'src', 'admin',  'components')))
    app.use('/admin/common', express.static(path.join(process.cwd(), 'src', 'admin', 'common')))
    app.use('/admin/mixins', express.static(path.join(process.cwd(), 'src', 'admin', 'mixins')))

    app.get('/worker-javascript.js', async (req, res) => {
        const mime = require('mime');
        res.setHeader("Content-Type", mime.getType(req.url))
        res.send((await require('axios').get('https://pagecdn.io/lib/ace/1.4.12/worker-javascript.min.js')).data)
    })

    api(app)

    app.use((req, res, next) => {
        console.log('REQ',req.url,req.query)
        if (req.url.indexOf('.') !== -1) {
            console.log('404!')
            res.status(404).send()
        }else{
            next()
        }
    })


    app.use('/admin*', (req, res, next) => {
        
        
        res.sendFile(path.join(process.cwd(), 'src/admin/public/index.html'))
    })

    app.use(async (req, res, next) => {

        let mongoose = require('mongoose')
        var m = n => mongoose.model(n)
        let url = req.url
        url = url.split('?')[0]
        if(url.length>1 && url.charAt(url.length-1)==='/'){
            url = url.substr(0, url.length-1)
        }
        let matched = await m('Page').findOne({
            isCore: {
                $ne: true
            },
            isTemplate: {
                $ne: true
            },
            path: {
                $eq: url
            }
        })
        console.log('page middleware', url, !!matched)
        if (matched) {
            let settings = await m('Page').findOne({
                name: "_settings"
            })
            let options = n => (settings.sections.find(s => s.name == n) || {}).value || ""
            let pageTemplate = options('default_page_template')
            matched = matched.toJSON()
            matched.baseTemplate = pageTemplate
            let template = matched.template || matched.baseTemplate
            let theme = options('theme')
            let pug = require('pug')

            let pagesDocs = (await m('Page').find({})).map(p => p.toJSON())
            let pages = {}
            pagesDocs.forEach(p => {
                let sections = p.sections
                p.sections = {}
                sections.forEach(s => p.sections[s.name] = s)
                pages[p.name] = p
            })

            let basedir = require('path').join(process.cwd(), 'templates')
            try {
                let html = pug.compileFile(`${basedir}/${theme}/${template.split('.pug').join('')}.pug`, {
                    basedir
                })({
                    pages,
                    matched: pages[matched.name],
                    pagesWithTag(tag) {
                        return pagesDocs.filter(p => (p.tags || []).map(t => t.toLowerCase()).includes(tag.toLowerCase())).map(p => pages[p.name])
                    }
                })
                console.log('pug compile', html.length,'lines')
                res.send(html)
            } catch (err) {
                app.io.emit('error', {
                    stack: err.stack,
                    date: Date.now()
                })
                console.log(err)
                if(req.query.pugErrors){
                    return res.status(200).send(err.stack.split('\n').join('<br/>'))
                }
                return res.status(404).send("Ups.. this page contains errors")
            }


        } else {
            res.status(404).send()
        }
    })



    const server = require('http').createServer(app);
    const options = { /* ... */ };
    const io = require('socket.io')(server, options);
    app.io = io
    io.on('connection', socket => {
        console.log('Client connected')
    });

    server.listen(PORT, async () => {
        console.log(`Listening on ${PORT}`)
    })

    app.api.loadFixtures()
})()